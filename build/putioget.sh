#!/bin/sh

source /app/bin/activate

: ${PUTIO_OAUTH_TOKEN=$(cat /var/run/secrets/PUTIO_OAUTH_TOKEN)}
: ${RPC_SECRET=$(cat /var/run/secrets/RPC_SECRET)}

export PUTIO_OAUTH_TOKEN
export RPC_SECRET

while true; do
    date
    LOG_LEVEL=info python /app/putioget.py
    echo 'Sleeping for 5m'
    sleep 300
done
