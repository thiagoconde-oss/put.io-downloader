#!/bin/bash
umask 0002

source /app/bin/activate

: ${JELLYFIN_TOKEN=$(cat /var/run/secrets/JELLYFIN_TOKEN)}
: ${OMDBAPI_KEY=$(cat /var/run/secrets/OMDBAPI_KEY)}

export JELLYFIN_TOKEN
export OMDBAPI_KEY

function update_jellyfin() {
    if [ -z "${JELLYFIN_TOKEN}" ]; then
        echo "JELLYFIN_TOKEN is empty"
        exit 1
    else
        curl -v -d "" -H "X-MediaBrowser-Token: ${JELLYFIN_TOKEN}" http://192.168.1.65/library/refresh
    fi
}

while true; do
    date
    if [[ -f /tv/.is_mounted ]]; then
      LOG_LEVEL=info python /app/movemedia.py && update_jellyfin
    else
      echo '/tv/ not mounted; skipping'
    fi
    echo 'Sleeping for 3m'
    sleep 180
done
